import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.jsx'
import MainArticle from './components/MainArticle.jsx'
import './index.css'

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <MainArticle></MainArticle>
  </React.StrictMode>,
)
