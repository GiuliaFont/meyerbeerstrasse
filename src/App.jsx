import { useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import './App.css'
import MainArticle from './components/MainArticle'
function App() {
  const [count, setCount] = useState(0)

  return (
    <>
      <div>
        <MainArticle></MainArticle>
      </div>
      
    </>
  )
}

export default App
